<?php

namespace Novalnet\Bundle\NovalnetBundle\Settings\DataProvider;

/**
 * Novalnet Payment Settings Dataproider
 */
 
class BasicNovalnetSettingsDataProvider implements NovalnetSettingsDataProviderInterface
{
    /**
     * @internal
     */
    const CAPTURE = 'capture';

    /**
     * @internal
     */
    const AUTHORIZE = 'authorize';

    /**
     * @internal
     */
    const TWO = '2';

    /**
     * @internal
     */
    const THREE = '3';

    /**
     * @internal
     */
    const FOUR = '4';
    
    /**
     * @internal
     */
    const FIVE = '5';

    /**
     * @internal
     */
    const SIX = '6';
    
    /**
     * @internal
     */
    const SEVEN = '7';
    
    /**
     * @internal
     */
    const EIGHT = '8';
    
    /**
     * @internal
     */
    const NINE = '9';
   
    /**
     * @internal
     */
    const TEN = '10';
    
    /**
     * @internal
     */
    const ELEVEN = '11';

    /**
     * @internal
     */
    const TWELVE = '12';

    /**
     * @internal
     */
    const FIFTEEN = '15';
    /**
     * @internal
     */
    const EIGHTEEN = '18';
    /**
     * @internal
     */
    const TWENTY_ONE = '21';

    /**
     * @internal
     */
    const TWENTY_FOUR = '24';

    /**
     * @internal
     */
    const VISA = 'visa';

    /**
     * @internal
     */
    const MASTERCARD = 'mastercard';

    /**
     * @internal
     */
    const MAESTRO = 'maestro';

    /**
     * @internal
     */
    const AMERICAN_EXPRESS = 'amex';

    /**
     * @internal
     */
    const UNIONPAY = 'unionpay';

    /**
     * @internal
     */
    const DISCOVER = 'discover';

    /**
     * @internal
     */
    const DINERS = 'diners';

    /**
     * @internal
     */
    const CARTE_BLEUE = 'carte_bleue';

    /**
     * @internal
     */
    const CARTASI = 'cartasi';

    /**
     * @internal
     */
    const JCB = 'jcb';

    /**
     * @return string[]
     */
    public function getInstalmentCycle()
    {
        return [
            self::TWO,
            self::THREE,
            self::FOUR,
            self::FIVE,
            self::SIX,
            self::SEVEN,
            self::EIGHT,
            self::NINE,
            self::TEN,
            self::ELEVEN,
            self::TWELVE,
            self::FIFTEEN,
            self::EIGHTEEN,
            self::TWENTY_ONE,
            self::TWENTY_FOUR,
        ];
    }

    /**
     * @return string[]
     */
    public function getPaymentActions()
    {
        return [
            self::CAPTURE,
            self::AUTHORIZE,
        ];
    }

    /**
     * @return string[]
     */
    public function getCreditCardLogos()
    {
        return [
            self::VISA,
            self::MASTERCARD,
            self::MAESTRO,
            self::AMERICAN_EXPRESS,
            self::UNIONPAY,
            self::DISCOVER,
            self::DINERS,
            self::CARTE_BLEUE,
            self::CARTASI,
            self::JCB,
        ];
    }
}
